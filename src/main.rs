mod structs;

use std::{
    env,
    fs::File,
    io::{BufRead, BufReader, Write},
};

use crate::structs::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: {} <filename>", args[0]);
        return;
    }

    let gcode = File::open(&args[1]).expect(&format!("Error opening {}!", &args[1]));
    let reader = BufReader::new(gcode);

    let mut data: Vec<ToolpathCommand> = Vec::new();
    let mut initial_temp: Option<i32> = None;

    let (mut prev_feedrate, mut prev_x, mut prev_y, mut prev_z, mut prev_a): (
        f64,
        f64,
        f64,
        f64,
        f64,
    ) = (0.0, 0.0, 0.0, 0.0, 0.0);
    for line in reader.lines() {
        let line = line.expect(&format!("Error reading {}!", &args[1]));
        if &line[..1] != "G" && &line[..1] != "M" {
            continue;
        }

        let mut cmds = line.split_whitespace();
        let mut command = Command::new();
        let (mut feedrate, mut x, mut y, mut z, mut a): (f64, f64, f64, f64, f64) =
            (prev_feedrate, prev_x, prev_y, prev_z, prev_a);
        match &cmds.nth(0).unwrap()[1..] {
            "0" | "1" => {
                command.function = String::from("move");
                for cmd in cmds {
                    match &cmd[..1] {
                        "F" => {
                            feedrate = gcode_arg(cmd) / 60.0;
                            prev_feedrate = gcode_arg(cmd) / 60.0;
                        }
                        "X" => {
                            x = gcode_arg(cmd);
                            prev_x = gcode_arg(cmd);
                        }
                        "Y" => {
                            y = gcode_arg(cmd);
                            prev_y = gcode_arg(cmd);
                        }
                        "Z" => {
                            z = gcode_arg(cmd);
                            prev_z = gcode_arg(cmd);
                        }
                        "E" => {
                            a = gcode_arg(cmd);
                            prev_a = gcode_arg(cmd);
                        }
                        _ => {}
                    };
                }
                command.parameters = Param::Move {
                    feedrate,
                    a,
                    x,
                    y,
                    z,
                };
            }
            "104" => {
                command.function = String::from("set_toolhead_temperature");
                let temp = cmds.nth(0).unwrap();

                if &temp[..1] != "S" {
                    continue;
                }

                if initial_temp.is_none() {
                    initial_temp = Some(gcode_arg(temp) as i32);
                }
                command.parameters = Param::ToolheadTemp {
                    temperature: gcode_arg(temp) as i32,
                };
            }
            _ => continue,
        }

        data.push(ToolpathCommand { command });
    }

    let output = File::create("output.makerbot").expect("Failed to create ZIP!");
    let mut zip = zip::ZipWriter::new(output);

    zip.start_file("meta.json", Default::default())
        .expect("Failed to create ZIP!");
    zip.write_all(
        serde_json::to_string_pretty(&MetaFile {
            printer_settings: PrinterSettings {
                extruder: String::from("0"),
                extruder_temperature: initial_temp,
                extruder_temperatures: vec![initial_temp],
            },
            total_commands: data.len(),
            toolhead_0_temperature: initial_temp,
        })
        .unwrap()
        .as_bytes(),
    )
    .expect("Failed to create ZIP!");

    zip.start_file("print.jsontoolpath", Default::default())
        .expect("Failed to create ZIP!");
    zip.write_all(serde_json::to_string_pretty(&data).unwrap().as_bytes())
        .expect("Failed to create ZIP!");
}

fn gcode_arg(arg: &str) -> f64 {
    arg[1..].parse().unwrap()
}
